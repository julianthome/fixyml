# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  # The context of these variables is global to the downstream job
  DOCKER_DRIVER: overlay2
  MAJOR: 1
  TMP_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA

stages:
  - maintenance      # update the project dependencies; only if MAINTENANCE is set
  - pre-build        # lint code, run unit tests, and compile binaries
  - build-package    # build distro package(s) for the analyzer and/or its dependencies
  - build-image      # build the Docker image(s) for the analyzer
  - test             # check, test, and scan the Docker images
  - performance-metrics # run performance checks
  - release-version  # release Docker images and distro packages
  - release-major    # update Docker images and distro packages of the major release
  - post             # run benchmarks

.if-gitlab-org-branch: &if-gitlab-org-branch
  if: '$CI_PROJECT_NAMESPACE == "gitlab-org/security-products/analyzers"'

# Only run for gitlab-org branches, as downstream needs exec permissions
.downstream_rules: &downstream_rules
  - if: $DOWNSTREAM_DISABLED
    when: never
  - <<: *if-gitlab-org-branch
  - when: never

.maintenance:
  stage: maintenance
  rules:
    - if: $MAINTENANCE
      when: always
    - when: never

maintenance_blocker:
  extends: .maintenance
  allow_failure: false
  script: echo "Dummy job to block the rest of the pipeline" && false

update_common:
  extends: .maintenance
  image: golang:$GO_VERSION
  variables:
    TITLE: "Upgrade to common ${COMMON_COMMIT_TAG}"
    TARGET_BRANCH: "$CI_COMMIT_REF_SLUG"
    SOURCE_BRANCH: "upgrade-to-common-$COMMON_COMMIT_TAG-$CI_JOB_ID"
  script: |
     set -x
     git config --global user.email "gitlab-bot@gitlab.com"
     git config --global user.name "GitLab Bot"
     git checkout -b $SOURCE_BRANCH
     go get gitlab.com/gitlab-org/security-products/analyzers/common/v2@${COMMON_COMMIT_TAG}
     go mod tidy
     git diff --quiet go.mod && exit 1
     git commit -m "$TITLE" go.sum go.mod
     git push -o merge_request.create -o merge_request.remove_source_branch -o merge_request.title="$TITLE" -o merge_request.label="backend" -o merge_request.label="devops::secure" -o merge_request.target="$TARGET_BRANCH" ${CI_PROJECT_URL/https:\/\/gitlab.com/https://gitlab-bot:$GITLAB_TOKEN@gitlab.com}.git $SOURCE_BRANCH

build tmp image:
  image: docker:20.10
  stage: build-image
  services:
    - docker:20.10-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg GO_VERSION -t $TMP_IMAGE .
    - docker push $TMP_IMAGE

# check image size ensures that the total size of the compressed image layers
# is lower than MAX_IMAGE_SIZE_BYTE. This size corresponds to what is
# transferred between the Container Registry of the analyzer
# and the CI when running the scanning job.
check image size:
  stage: test
  variables:
    PATH_NAME: "tmp"
  image: alpine:3.12
  script:
    - apk add --no-cache jq
    - "repository_id=$(wget -q -O- --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories\" | jq --arg NAME ${PATH_NAME} '.[] | select(.name == $NAME) | .id')"
    - "current_size=$(wget -q -O- --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${repository_id}/tags/${CI_COMMIT_SHA}\" | jq '.total_size')"
    - echo total size of the compressed image layers, in bytes
    - echo $current_size
    - if [ -n "${MAX_IMAGE_SIZE_MB}" ]; then export MAX_IMAGE_SIZE_BYTE=$(expr $MAX_IMAGE_SIZE_MB \* 1024 \* 1024); fi
    - allowed_image_size=${MAX_IMAGE_SIZE_BYTE:-419430400}
    - echo $allowed_image_size
    - test $allowed_image_size -gt $current_size

check analyzer version:
  stage: test
  image: docker:20.10
  stage: test
  services:
    - docker:20.10-dind
  script:
    - ANALYZER_VERSION_STRING=$(docker run ${TMP_IMAGE} /analyzer --version || true)
    - CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "${CI_PROJECT_DIR}/CHANGELOG.md" | sed 's/## v//')
    - ANALYZER_VERSION=$(echo "$ANALYZER_VERSION_STRING" | sed -E -n -e 's/^\S+ \S+ (\d+\.\d+\.\d+)/\1/p')
    - infomsg="Please update the analyzer binary version to match the most recent version in CHANGELOG.md."
    - |
      if [ "$ANALYZER_VERSION" == "0.0.0" ] || echo "$ANALYZER_VERSION_STRING" | grep -q "flag provided but not defined: -version"; then
        echo "Error: analyzer binary does not have a version configured. $infomsg"
        exit 1
      elif [ "$CHANGELOG_VERSION" != "$ANALYZER_VERSION" ]; then
        echo "Error: The most recent version in CHANGELOG.md '$CHANGELOG_VERSION' does not match the analyzer version value '$ANALYZER_VERSION'. $infomsg"
        exit 1
      else
        echo "Success: Analyzer binary version '$ANALYZER_VERSION' matches CHANGELOG.md version '$CHANGELOG_VERSION'"
      fi

test-custom-ca-bundle:
  rules: *downstream_rules
  stage: test
  variables:
    BASE_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA
    TARGET: $CI_PROJECT_NAME
    SECURE_LOG_LEVEL: debug
  trigger:
    project: "gitlab-org/security-products/tests/custom-ca"
    branch: master
    strategy: depend

.docker_tag:
  image: docker:20.10
  stage: release-version
  services:
    - docker:20.10-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export SOURCE_IMAGE=$TMP_IMAGE
    - export TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}
    - docker pull $SOURCE_IMAGE
    - docker tag $SOURCE_IMAGE $TARGET_IMAGE
    - docker push $TARGET_IMAGE

.qa-downstream-ds:
  rules: *downstream_rules
  stage: test
  variables:
    SAST_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SAST_DEFAULT_ANALYZERS: ""
    DS_DEFAULT_ANALYZERS: ""
    DS_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    DS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-sast:
  rules: *downstream_rules
  stage: test
  variables:
    DEPENDENCY_SCANNING_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    DS_DEFAULT_ANALYZERS: ""
    SAST_DEFAULT_ANALYZERS: ""
    SAST_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_DISABLE_DIND: "false"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-cs:
  rules: *downstream_rules
  stage: test
  variables:
    SAST_DISABLED: "true"
    DEPENDENCY_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SECURE_LOG_LEVEL: debug
    CS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
  trigger:
    project: ""
    branch: master
    strategy: depend

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle non-alphanumeric
    # characters, but this may limit our tags to 63chars or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $CI_COMMIT_BRANCH

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME

tag version:
  extends: .docker_tag
  before_script:
    - export RELEASE=${CI_COMMIT_TAG/+*/}
    - export IMAGE_TAG=${RELEASE/v/}
    - echo "Checking that $RELEASE is last in the changelog"
    - test "$(grep -m 1 '^## v' CHANGELOG.md)" = "## $RELEASE"
  rules:
    - if: $CI_COMMIT_TAG

.release:
  extends: .docker_tag
  stage: release-major
  rules:
    - if: $CI_COMMIT_TAG

major:
  extends: .release
  variables:
    IMAGE_TAG: $MAJOR

minor:
  extends: .release
  before_script:
    - export RELEASE=${CI_COMMIT_TAG/+*/}
    # Cut off last dot and whatever follows it
    - export IMAGE_TAG=${RELEASE%.*}
    # Drop leading v
    - export IMAGE_TAG=${IMAGE_TAG/v/}
  rules:
    - if: $CI_COMMIT_TAG

latest:
  extends: .release

